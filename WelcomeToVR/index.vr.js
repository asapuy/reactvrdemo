import React from "react";
import { get3DPoint } from "./vr/cameraHelper.js";
import {
  AppRegistry,
  Animated,
  asset,
  Image,
  Pano,
  Text,
  Model,
  Sphere,
  View,
  Video,
  VrButton
} from "react-vr";

export default class WelcomeToVR extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      source: {
        uri:
          "https://s3-us-west-2.amazonaws.com/reactvr-files/PANO_20170530_160639.jpg"
      },
      videoTransform: {
        width: 2.8,
        height: 2,
        transform: [
          { translate: [3.0, -1.7, 4.5] },
          { rotateY: 35 },
          { rotateZ: 5 }
        ]
      },
      videoVolume: 1,
      rotation: 130,
      bounceValue: new Animated.Value(0),
      doubleClickObjects: []
    };

    this.doubleClicks = [];
    this.lastUpdate = Date.now();
    this.clicked = false;
  }

  descriptionClicked() {
    this.clicked = !this.clicked;
    this.animateStart();
  }

  changeImage(img) {
    if (
      img ==
      "https://s3-us-west-2.amazonaws.com/reactvr-files/PANO_20170530_160346.jpg"
    ) {
      this.setState({
        source: { uri: img },
        videoTransform: {
          width: 3.2,
          height: 2.4,
          transform: [
            { translate: [10.0, -5, -34.5] },
            { rotateY: 35 },
            { rotateZ: -15 }
          ]
        },
        videoVolume: 0.2
      });
    } else {
      this.setState({
        source: { uri: img },
        videoTransform: {
          width: 2.8,
          height: 2,
          transform: [
            { translate: [3.0, -1.7, 4.5] },
            { rotateY: 35 },
            { rotateZ: 5 }
          ]
        },
        videoVolume: 0.7
      });
    }
  }

  componentWillMount() {
    window.addEventListener("message", this.onMainWindowMessage);
  }
  componentDidMount() {
    //this.rotate();
    //window.ondblclick = this.onRendererDoubleClick.bind(this);
  }

  onMainWindowMessage = e => {
    if (e.data.constructor === String) {
      var message = JSON.parse(e.data);
      switch (message.type) {
        case "newCoordinates":
          let newDoubleClicks = [...this.state.doubleClickObjects];
          newDoubleClicks.push([
            message.coordinates.x,
            message.coordinates.y,
            message.coordinates.z
          ]);
          this.setState({ doubleClickObjects: newDoubleClicks });
          break;
        default:
          return;
      }
    }
  };

  animateStart() {
    this.state.bounceValue.setValue(1.5); // Start large
    Animated.spring(
      // Base: spring, decay, timing
      this.state.bounceValue, // Animate `bounceValue`
      {
        toValue: 0.9, // Animate to smaller size
        friction: 0.1 // Bouncier spring
      }
    ).start();
  }
  animateStop() {
    this.state.bounceValue.setValue(1.5); // Start large
    Animated.spring(
      // Base: spring, decay, timing
      this.state.bounceValue, // Animate `bounceValue`
      {
        toValue: 0.8, // Animate to smaller size
        friction: 0.01 // Bouncier spring
      }
    ).start();
  }

  rotate = () => {
    console.log("rotate");
    const now = Date.now();
    const delta = now - this.lastUpdate;
    this.lastUpdate = now;
    this.setState({ rotation: this.state.rotation + delta / 120 });
    this.frameHandle = requestAnimationFrame(this.rotate);
  };

  onNavigationClick = (item, e) => {
    if (
      e.nativeEvent.inputEvent.eventType === "mousedown" &&
      e.nativeEvent.inputEvent.button === 0
    ) {
      var new_scene = this.state.scenes.find(i => i["step"] === item.step);
      this.setState({ current_scene: new_scene });
      postMessage({ type: "sceneChanged" });
    }
  };

  render() {
    console.log("render");
    var body;
    var button;
    if (
      this.state.source.uri ==
      "https://s3-us-west-2.amazonaws.com/reactvr-files/PANO_20170530_160346.jpg"
    ) {
      if (this.clicked) {
        button = (
          <View style={{ position: "absolute" }}>
            <VrButton
              style={{
                backgroundColor: "rgba(100,149,237, 0.9)",
                width: "150px",

                layoutOrigin: [0, 0],
                height: 1,
                width: 2,
                paddingLeft: 0.2,
                paddingRight: 0.2,

                transform: [
                  { translate: [-3, 0.7, 1.8] },
                  { scale: 1 },
                  { rotateY: 155 }
                ]
              }}
              onClick={() => this.descriptionClicked()}
            >
              <Animated.Text
                style={{
                  color: "rgb(120,153,34)",
                  fontSize: 0.14,
                  fontWeight: "400",
                  textAlign: "center",
                  textAlignVertical: "center",
                  transform: [
                    // `transform` is an ordered array
                    { scale: this.state.bounceValue } // Map `bounceValue` to `scale`
                  ]
                }}
              >
                This is gonzalo one of our experienced Front-end developers.
              </Animated.Text>

              <View style={{ flexDirection: "row" }}>
                <Animated.Image
                  style={{
                    height: 0.34,
                    width: 0.34,
                    transform: [
                      // `transform` is an ordered array
                      { scale: this.state.bounceValue } // Map `bounceValue` to `scale`
                    ]
                  }}
                  source={{
                    uri:
                      "https://i2.wp.com/www.asapdevelopers.com/wp-content/uploads/2017/04/gonzalo-larrosa.jpg?fit=500%2C600"
                  }}
                />
              </View>
            </VrButton>
          </View>
        );
      } else {
        button = (
          <View style={{ position: "absolute" }}>
            <VrButton
              style={{
                layoutOrigin: [0, 0],
                transform: [{ translate: [-3, 0.7, 1.8] }, { scale: 1 }]
              }}
              onClick={() => this.descriptionClicked()}
            >
              <Image
                style={{ height: 0.34, width: 0.34 }}
                source={asset("Plumbob.png")}
              />
            </VrButton>
          </View>
        );
      }

      body = (
        <View style={{ position: "absolute" }}>
          <VrButton
            style={{
              backgroundColor: "rgba(76, 175, 80, 0)",
              layoutOrigin: [0, 0],
              paddingLeft: 0.2,
              paddingRight: 0.2,
              transform: [{ translate: [-1, 0, -3] }, { rotateX: -15 }]
            }}
            onClick={() =>
              this.changeImage(
                "https://s3-us-west-2.amazonaws.com/reactvr-files/PANO_20170530_160639.jpg"
              )
            }
          >
            <Text
              style={{
                backgroundColor: "rgba(76, 175, 80, 0.1)",
                fontSize: 0.8,
                fontWeight: "400",
                layoutOrigin: [0, 0],
                paddingLeft: 0.2,
                paddingRight: 0.2,
                textAlign: "center",
                textAlignVertical: "center",
                transform: [{ translate: [0, 1, -3] }, { rotateX: -35 }]
              }}
            >
              Back
            </Text>
          </VrButton>
        </View>
      );
    } else {
      body = (
        <View style={{ position: "absolute" }}>
          <VrButton
            style={{
              backgroundColor: "rgba(76, 175, 80, 0)",
              layoutOrigin: [0, 0],
              paddingLeft: 0.2,
              paddingRight: 0.2,
              transform: [{ translate: [-6, 0, -2] }]
            }}
            onClick={() =>
              this.changeImage(
                "https://s3-us-west-2.amazonaws.com/reactvr-files/PANO_20170530_160346.jpg"
              )
            }
          >
            <Text
              style={{
                backgroundColor: "rgba(76, 175, 80, 0.1)",
                fontSize: 0.8,
                fontWeight: "400",
                layoutOrigin: [0, 0],
                paddingLeft: 0.2,
                paddingRight: 0.2,
                textAlign: "center",
                textAlignVertical: "center",
                transform: [
                  { translate: [-6, 0, -2] },
                  { scale: 5 },
                  { rotateY: -35 }
                ]
              }}
            >
              ^
            </Text>
          </VrButton>
        </View>
      );
    }

    const { doubleClickObjects } = this.state;
    return (
      <View>
        <Pano source={this.state.source} style={{ position: "absolute" }} />
        {body}
        {button}
        {doubleClickObjects.length > 0 &&
          doubleClickObjects.map(item => {
            return (
              <View key={item}>
                <Model
                  style={{
                    transform: [
                      { translate: [item[0], item[1], item[2]] },
                      { rotateY: 100 /*this.state.rotation*/ },
                      { rotateX: 20 },
                      { rotateZ: -10 },
                      { scale: 5 }
                    ],
                    position: "absolute"
                  }}
                  source={{
                    obj:
                      "https://s3-us-west-2.amazonaws.com/reactvr-files/IPhone4Gs/IPhone+4Gs+_5.obj",
                    mtl:
                      "https://s3-us-west-2.amazonaws.com/reactvr-files/IPhone4Gs/IPhone+4Gs+_5.mtl"
                  }}
                />
              </View>
            );
          })}
        <Video
          volume={this.state.videoVolume}
          style={this.state.videoTransform}
          source={{
            uri: "https://s3-us-west-2.amazonaws.com/reactvr-files/hello.webm"
          }}
        />
      </View>
    );
  }
}
//  <Animated.Image  style={{flex:2,height:0.34,width:0.34,
//        transform: [                        // `transform` is an ordered array
//          {scale: this.state.bounceValue},  // Map `bounceValue` to `scale`
//        ]}} source={asset('balofa.jpg')}
//         />
AppRegistry.registerComponent("WelcomeToVR", () => WelcomeToVR);
